package implementation;

import jdk.nashorn.internal.runtime.regexp.joni.exception.ValueException;
import org.bouncycastle.asn1.ASN1InputStream;
import org.bouncycastle.asn1.ASN1Sequence;
import org.bouncycastle.asn1.DEROctetString;
import org.bouncycastle.asn1.x500.X500Name;
import org.bouncycastle.asn1.x509.*;
import org.bouncycastle.cert.X509CertificateHolder;
import org.bouncycastle.cert.jcajce.JcaX509CertificateConverter;
import org.bouncycastle.x509.X509V3CertificateGenerator;

import javax.security.auth.x500.X500Principal;
import java.math.BigInteger;
import java.security.*;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.*;

/**
 * Created by rols on 6/10/17.
 */
public class Helper {

    private static final Integer[] dsaLengths = { 512, 576, 640, 704, 768, 832, 896, 960, 1024, 2048};

    public static KeyPair generateDSAKeypair(Integer length) throws NoSuchProviderException, NoSuchAlgorithmException, ValueException {

        if (!Arrays.asList(dsaLengths).contains(length)) {
            throw new ValueException("Not allowed dsa size!");
        }

        KeyPairGenerator keyGen = KeyPairGenerator.getInstance("DSA", "BC");
        keyGen.initialize(length, new SecureRandom());
        KeyPair keyPair = keyGen.generateKeyPair();

        return keyPair;
    }

    public static X509Certificate generateCertificate(
            KeyPair pair,
            String serialNumber,
            X500Principal issuerDN,
            Date from,
            Date to,
            X500Principal subjectDN,
            String signatureAlgorithm,
            int basicConstraints,
            boolean basicConstraintsCritical,
            KeyUsage keyUsage,
            boolean keyUsageCritical,
            GeneralNames issuerAlternativeNames,
            boolean issuerAlternativeNamesCritical
    ) throws SignatureException, NoSuchProviderException, InvalidKeyException {
        X509V3CertificateGenerator certGen = new X509V3CertificateGenerator();

        certGen.setSerialNumber(new BigInteger(serialNumber));
        certGen.setIssuerDN(issuerDN);
        certGen.setNotBefore(from);
        certGen.setNotAfter(to);
        certGen.setSubjectDN(subjectDN);
        certGen.setPublicKey(pair.getPublic());
        certGen.setSignatureAlgorithm(signatureAlgorithm);

        certGen.addExtension(
                X509Extensions.BasicConstraints,
                basicConstraintsCritical,
                basicConstraints==-1? new BasicConstraints(false):new BasicConstraints(basicConstraints));

        certGen.addExtension(X509Extensions.KeyUsage, keyUsageCritical, keyUsage);//new KeyUsage(KeyUsage.digitalSignature | KeyUsage.keyEncipherment));

        certGen.addExtension(X509Extensions.IssuerAlternativeName, issuerAlternativeNamesCritical, issuerAlternativeNames);//new GeneralNames(new GeneralName(GeneralName.rfc822Name, "test@test.test")));

        return certGen.generateX509Certificate(pair.getPrivate(), "BC");
    }


    public static X500Principal getPrincipal(
            String country,
            String state,
            String locality,
            String organization,
            String organizationUnit,
            String commonName
    ) {
        StringBuilder sb = new StringBuilder();
        if (!country.equals("")) {
            sb.append("C=");
            sb.append(country);
            sb.append(",");
        }
        if (!state.equals("")) {
            sb.append("ST=");
            sb.append(state);
            sb.append(",");
        }
        if (!locality.equals("")) {
            sb.append("L=");
            sb.append(locality);
            sb.append(",");
        }
        if (!organization.equals("")) {
            sb.append("O=");
            sb.append(organization);
            sb.append(",");
        }
        if (!organizationUnit.equals("")) {
            sb.append("OU=");
            sb.append(organizationUnit);
            sb.append(",");
        }
        if (!commonName.equals("")) {
            sb.append("CN=");
            sb.append(commonName);
            sb.append(",");
        }

        sb.setLength(sb.length()-1);

        return new X500Principal(sb.toString());
    }

//    public static X509Certificate[] convertChain(Certificate[] chain) throws CertificateException {
//        X509Certificate newChain[] = new X509Certificate[chain.length];
//        for (int i = 0 ; i < chain.length; i++) {
//            X509Certificate newCert = (X509Certificate) chain[i];
//            newChain[i] = newCert;
//        }
//
//        return newChain;
//    }

//    public static boolean isCertificateAuthority(X509Certificate cert){
//        try {
//            byte[] basicConstraints=cert.getExtensionValue("2.5.29.19");
//            Object obj=new ASN1InputStream(basicConstraints).readObject();
//            basicConstraints=((DEROctetString)obj).getOctets();
//            obj=new ASN1InputStream(basicConstraints).readObject();
//            return BasicConstraints.getInstance((ASN1Sequence)obj).isCA();
//        }
//        catch (  Exception e) {
//            return false;
//        }
//    }

    public static int getKeyUsage(boolean usages[]) {
        int usage = 0;

        if (usages[0]) usage |= KeyUsage.digitalSignature;
        if (usages[1]) usage |= KeyUsage.nonRepudiation;
        if (usages[2]) usage |= KeyUsage.keyEncipherment;
        if (usages[3]) usage |= KeyUsage.dataEncipherment;
        if (usages[4]) usage |= KeyUsage.keyAgreement;
        if (usages[5]) usage |= KeyUsage.keyCertSign;
        if (usages[6]) usage |= KeyUsage.cRLSign;
        if (usages[7]) usage |= KeyUsage.encipherOnly;
        if (usages[8]) usage |= KeyUsage.decipherOnly;

        return usage;
    }

    public static GeneralNames getIssuerAlternativeNames(String names[]) {

        GeneralName generals[] = new GeneralName[names.length];

        for (int i = 0; i < names.length; i ++ ){
            String split[] = names[i].split(":");
            switch(split[0]) {
                case "otherName": generals[i] = new GeneralName(GeneralName.otherName,split[1]);break;
                case "rfc822Name": generals[i] = new GeneralName(GeneralName.rfc822Name,split[1]);break;
                case "dNSName": generals[i] = new GeneralName(GeneralName.dNSName,split[1]);break;
                case "x400Address": generals[i] = new GeneralName(GeneralName.x400Address,split[1]);break;
                case "directoryName": generals[i] = new GeneralName(GeneralName.directoryName,split[1]);break;
                case "ediPartyName": generals[i] = new GeneralName(GeneralName.ediPartyName,split[1]);break;
                case "uniformResourceIdentifier": generals[i] = new GeneralName(GeneralName.uniformResourceIdentifier,split[1]);break;
                case "iPAddress": generals[i] = new GeneralName(GeneralName.iPAddress,split[1]);break;
                case "registeredID": generals[i] = new GeneralName(GeneralName.registeredID,split[1]);break;
            }
        }

        return new GeneralNames(generals);
    }

    public static GeneralNames getIssuerAlternativeNames(Collection<List<?>> names) {
        List<GeneralName> generals = new ArrayList<GeneralName>();

        if (names != null)
        {
            Iterator it = names.iterator();
            while (it.hasNext())
            {
                // look for URI
                List list = (List)it.next();
                //if (list.get(0).equals(new Integer(GeneralName.uniformResourceIdentifier)))
                //{
                    // found
                    String temp = (String)list.get(1);
                    Integer type = (Integer)list.get(0);
                    generals.add( new GeneralName(type, temp));
                //}
            }
        }

        GeneralName generalsNames[] = new GeneralName[generals.size()];

        for (int i = 0 ; i < generals.size(); i ++) {
            generalsNames[i] = generals.get(i);
        }

        return new GeneralNames(generalsNames);
    }

    public static String getIssuerAlternativeNamesString(Collection<List<?>> names) {
        StringBuilder sb = new StringBuilder();

        if (names != null)
        {
            Iterator it = names.iterator();
            while (it.hasNext())
            {
                // look for URI
                List list = (List)it.next();
                //if (list.get(0).equals(new Integer(GeneralName.uniformResourceIdentifier)))
                //{
                    // found
                    String temp = (String)list.get(1);
                    Integer type = (Integer)list.get(0);
                    switch (type) {
                        case GeneralName.otherName: sb.append("otherName");break;
                        case GeneralName.rfc822Name: sb.append("rfc822Name");break;
                        case GeneralName.dNSName: sb.append("dNSName");break;
                        case GeneralName.x400Address: sb.append("x400Address");break;
                        case GeneralName.directoryName: sb.append("directoryName");break;
                        case GeneralName.ediPartyName: sb.append("ediPartyName");break;
                        case GeneralName.uniformResourceIdentifier: sb.append("uniformResourceIdentifier");break;
                        case GeneralName.iPAddress: sb.append("iPAddress");break;
                        case GeneralName.registeredID: sb.append("registeredID");break;
                    }
                    sb.append(":");
                    sb.append(temp);
                    sb.append(",");
                //}
            }
        }
        if (sb.length() > 0)
            sb.setLength(sb.length()-1);

        return sb.toString();
    }
}
